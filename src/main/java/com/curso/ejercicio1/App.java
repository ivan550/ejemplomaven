package com.curso.ejercicio1;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import org.apache.commons.io.FileUtils;

/**
 * 	Programa creado para primer proyecto con Maven
 *	Crea un archivo y después lo lee y sobreescribe
 *	Cambiar ruta de creación de archivo
 *
 *	Se ejecuta el jar que se crea con las dependencias
 */
public class App {
	
	static final String RUTA_ARCHIVO = "C:\\cursos\\Java\\2019\\maven\\apps\\archivo1.txt";
	
	public static void main(String[] args) throws IOException {
		File myfile = new File(RUTA_ARCHIVO);
		String contentido = "1|2|3|4|5|6|7|8|9|0";
		File archivoCreado = crearArchivo(myfile, contentido);
		reemplazarCadena(archivoCreado);
	}

	public static File crearArchivo(File archivo1, String contentido) throws IOException {

		FileUtils.touch(archivo1);

		if (archivo1.exists()) {
			System.out.println("Archivo creado");
		} else {

			System.out.println("Archivo ya existe");
		}
		FileUtils.writeStringToFile(archivo1, contentido, StandardCharsets.UTF_8.name());
		return archivo1;
	}

	public static void reemplazarCadena(File archivo1) {
		try {
			String contentido = FileUtils.readFileToString(archivo1, StandardCharsets.UTF_8.name());
			contentido = contentido.replace("|", ",");
			FileUtils.writeStringToFile(archivo1, contentido, StandardCharsets.UTF_8.name());
		} catch (IOException e) {
			throw new RuntimeException("Generating file failed", e);
		}
	}
}
